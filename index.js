const express =require('express');
const mongoose =require('mongoose');
const cors = require('cors');
const app = express();
const port = 3000;

// ROUTES
const userRoutes = require('./routes/user')
const productRoutes = require('./routes/product')
const orderRoutes = require('./routes/order')



app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}))

app.use('/api/users', userRoutes)
app.use('/api/products', productRoutes)
app.use('/api/orders', orderRoutes)


mongoose.connect("mongodb+srv://dbUser:dbUser@wdc028-course-booking.0pav9.mongodb.net/ecommerce_api?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
})

mongoose.connection.once('open',() => console.log('Now Connected to MongoDB Atlas'))
app.listen(port, () => console.log(`Now listening to port ${port}`))


