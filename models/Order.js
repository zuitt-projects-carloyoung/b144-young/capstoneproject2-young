const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})

mongoose.exports = mongoose.model('Orders', orderSchema);