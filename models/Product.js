const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name:{
		type: String,
		required: [true, "Product Name is required"]
	},
	description:{
		type: String,
		required: [true, "Description of product is required"]
	},
	price:{
		type: Number,
		required: [true, "Price of product is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	purchasers:[
	{
		userId: {
			type: String,
			required: [true, 'userId is required']
		},
		boughtOn:{
			type: Date,
			default: new Date()
		}

	}]

})

module.exports = mongoose.model('Product', productSchema);