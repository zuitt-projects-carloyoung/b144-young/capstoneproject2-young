const Order = require("../models/Order")
const Product = require('../models/Product');
const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth')

// // Allow user to purchase a PRODUCT
module.exports.purchase = async (data) => {


	let isUserUpdated = await User.findById(data.userId).then(user =>{

		user.purchases.push({productId : data.productId})

		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		
		product.purchasers.push({userId : data.userId});

		return product.save().then((course,error) =>{
			if(error){
				return false;
			}
			else{
				return true
			}

		})
	})

	if(isUserUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}
} 

// Retrieve a specific user's order
module.exports.getOrder = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return result;
	})
}




// Retrieve all orders
// module.exports.getAllOrders = () => {
// 	return User.find({isAdmin : false}).then(result => {
// 		return result;
// 	})
// }
// module.exports.getAllOrders = () =>  {
// 	return Product.find({isAdmin :false}).then(result => {
// 		return result;
// 	})
// }

