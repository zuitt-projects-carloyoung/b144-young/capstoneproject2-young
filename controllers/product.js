const Product = require("../models/Product")

module.exports.addProduct = (reqBody, userData) => {
	return Product.findById(userData.userId).then(result =>{
		if (userData.isAdmin == false) {
			return "You are not an admin"
		} else {
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newProduct.save().then((product, error) => {
				if(error) {
					return false
				} else {
					return "Product Creation Complete!"
				}
			})
		}
	})
}

// Retrieve all Products
module.exports.getAllProducts = () =>  {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all Active Products
module.exports.getAllActive =() => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
} 

// Retrieve A Specific Product
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Update a Product
module.exports.updateProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price : reqBody.price
	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// Archive a Product
module.exports.archiveProduct = (reqParams) => {
	let archivedProduct = {
		isActive : false
	}
	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}