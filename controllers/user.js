const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth')

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0){
			return true;
		}else{
			return false;
		}
	})
}


// USER REGISTRATION
module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		email:reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}else{
			return true
		}
	})
}

// Authenticate ang login a user
module.exports.loginUser = (reqBody) => {
	
	return User.findOne({ email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password)
		
		if(isPasswordCorrect){
			return{ accessToken: auth.createAccessToken(result.toObject())}
		}else{
			return false
			}
		}
	})
}

// Get USER DETAILS
module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result =>{
		result.password ="";
		return result;
	});
}

// Get All USERS and their DETAILS
module.exports.getAllUsers = () => {
	return User.find({isAdmin: false}).then(result => {
		return result
	})
}


// Route for an admin user to designate a non-admin user as an admin

module.exports.updateUser = (reqParams, reqBody) => {
	let updatedUser = {
		isAdmin: reqBody.isAdmin
	}

	return User.findByIdAndUpdate(reqParams.userId, updatedUser).then((user,error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// // Allow user to purchase a PRODUCT
module.exports.checkout = async (data) => {


	let isUserUpdated = await User.findById(data.userId).then(user =>{

		user.myOrders.push({productId : data.productId})

		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	

	let isProductUpdated = await Product.findById(data.productId).then(product => {
		
		product.purchasers.push({userId : data.userId});

		return product.save().then((course,error) =>{
			if(error){
				return false;
			}
			else{
				return true
			}

		})
	})

	if(isUserUpdated && isProductUpdated){
		return true;
	}
	else{
		return false;
	}
} 

// Retrieve a specific user's order
module.exports.getOrder = (reqParams) => {
	return User.findById(reqParams.userId).then(result => {
		return result;
	})
}

// Get Authenticated User's Orders
module.exports.getOrders = (data) => {
	return User.findById(data.userId).then(result =>{
		result.password ="";
		return result;
	});
}
