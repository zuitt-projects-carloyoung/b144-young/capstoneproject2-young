const express = require("express");
const router = express.Router();
const orderController = require("../controllers/order");
const auth = require("../auth")


// // Route to allow USER to purchase a PRODUCT
// router.post("/purchase", auth.verify, (req,res) => {

// 	const userData = auth.decode(req.headers.authorization);

// 	let data = {
// 		userId: req.body.userId,
// 		productId: req.body.productId
// 	}

// 	orderController.purchase(data).then(
// 		resultFromController => res.send(resultFromController))
// })

// retrieve a specific user's order
router.get("/:userId", (req,res) => {
	console.log(req.params.userId);

	orderController.getOrder(req.params).then(resultFromController => res.send(resultFromController))
})



module.exports = router;