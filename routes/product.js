const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth")

// Create a Product

router.post("/", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization)

	productController.addProduct(req.body, {userId:userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

// Retrieve all Products
router.get("/allproducts", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send (resultFromController))
});

// Retrieve all active Products
router.get("/active", (req,res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
})

// Retrieve a specific Product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Update a PRODUCT
router.put("/:productId" , auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Archive a PRODUCT
router.put("/:productId/archive", auth.verify, (req,res) => {
	productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
})



module.exports = router;