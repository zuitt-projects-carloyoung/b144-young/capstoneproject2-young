const express = require('express')
const router = express.Router();
const auth = require("../auth")

const userController = require('../controllers/user')

// Route for checking if the user's email is already used in the database
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

// Route for new User Registration
router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result))
})

// Route for authenticating a user
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})

// Route for retrieving user details
router.get('/details', auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})

// Route for retrieving all user and their details
router.get("/orders", auth.verify, (req,res) => {
	
	const userData = auth.decode(req.headers.authorization);

	userController.getAllUsers().then(resultFromController => res.send(resultFromController));
})

// Route for an admin user to designate a non-admin user as an admin
router.put("/:userId/setAsAdmin" , auth.verify, (req, res) => {
	userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController))
})

// Route to allow an authenticated non-admin USER to purchase a PRODUCT
router.post("/checkout", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	let data = {
		userId: req.body.userId,
		productId: req.body.productId
	}

	userController.checkout(data).then(
		resultFromController => res.send(resultFromController))
})


// Route for retrieving authenticated user's orders
router.get("/myOrders", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	userController.getOrders({userId: userData.id}).then(resultFromController => res.send(resultFromController))
})






module.exports = router;